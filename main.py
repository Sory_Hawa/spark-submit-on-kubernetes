from pyspark import SparkContext
from pyspark.sql import SparkSession

spark = SparkSession.builder.getOrCreate()

def load_config(spark_context: SparkContext):
    spark_context._jsc.hadoopConfiguration().set('fs.s3a.access.key','minioadmin')
    spark_context._jsc.hadoopConfiguration().set('fs.s3a.secret.key','minioadmin')
    spark_context._jsc.hadoopConfiguration().set('fs.s3a.path.style.access','true')
    spark_context._jsc.hadoopConfiguration().set('fs.s3a.impl','org.apache.hadoop.fs.s3a.S3AFileSystem')
    spark_context._jsc.hadoopConfiguration().set('fs.s3a.endpoint','http://192.168.100.200:9000')
    spark_context._jsc.hadoopConfiguration().set('fs.s3a.connection.ssl.enable','false')
    spark_context._jsc.hadoopConfiguration().set('fs.s3a.aws.credentials.provider', 'org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider')


load_config(spark.sparkContext)

dataframe = spark.read.json('s3a://orders/*')

average = dataframe.agg({'amount': 'avg'})

average.show()